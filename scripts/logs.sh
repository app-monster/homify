#!/bin/bash

# Variable contains repos names we want to perform commands in
source ./constants/repos.sh

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

# Go to the homify root directory
cd "$DIR/../"

app=$1
if [ ! "${app}" ]; then
    echo "PLEASE SET A SERVICE TO GET LOGS"
else
    # use `docker` to tail logs to be able to pretty print json logs; `docker-compose` prepends service name preventing that.
	docker logs --tail 100 -f $app | ./node_modules/.bin/bunyan
fi