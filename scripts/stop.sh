#!/bin/bash

# Variable contains repos names we want to perform commands in
source ./constants/repos.sh

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

# Go to the homify root directory
cd "$DIR/../"

app=$1
cmd="docker-compose -f docker-compose.yml "

if [ -e "docker-compose.local.yml" ]; then
    cmd+="-f docker-compose.local.yml "
fi
cmd+="stop $app"

eval $cmd