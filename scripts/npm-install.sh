#!/bin/bash

# Variable contains repos names we want to perform commands in
source ./constants/repos.sh

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

# Go to the homify project's parent directory
cd "$DIR/../.."

# Installing dependencies for all repos
for i in "${repos[@]}"
do
    cd $i
    if [ ! -d "node_modules" ]; then
        echo "Installing packages for ${i}"
        npm i
    fi
done
