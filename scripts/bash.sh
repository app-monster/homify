#!/bin/bash

# Variable contains repos names we want to perform commands in
source ./constants/repos.sh

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

# Go to the homify root directory
cd "$DIR/../"

app=$1
docker-compose exec $app bash