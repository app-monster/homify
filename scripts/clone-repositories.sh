#!/bin/bash

# Variable contains repos names we want to perform commands in
source ./constants/repos.sh

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null && pwd )"

# Go to the homify project's parent directory
cd "$DIR/../.."

# Cloning only not found repos
for i in "${repos[@]}"
do
    if [ ! -d "${i}" ]; then
        echo "Cloning repo: ${i}"
		git clone https://cnajeraz@bitbucket.org/app-monster/$i.git
    fi
done
