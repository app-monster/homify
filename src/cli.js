import minimist from 'minimist';

import help from './help';
import version from './version';
import start from './up';
import stop from './stop';
import logs from './logs';
import bash from './bash';

export async function cli(argsArray) {
  const args = minimist(argsArray.slice(2));
  let cmd = args._[0] || 'help';

  if (cmd.version || cmd.v) {
    cmd = 'version';
  }

  if (cmd.help || cmd.h) {
    cmd = 'help';
  }

  switch (cmd) {
    case 'version':
      version(args);
      break;
    case 'help':
      help(args);
      break;
    case 'start':
      start(args._[1] || '');
      break;
    case 'stop':
      stop(args._[1] || '');
      break;
    case 'logs':
      logs(args._[1] || '');
      break;
    case 'login':
      bash(args._[1] || '');
      break;
    default:
      console.error(`"${cmd}" is not a valid command!`);
      break;
  }
}
