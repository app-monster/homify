import { spawn } from 'child_process';
import chalk from 'chalk';
import path from 'path';

export default (args) => {
  console.log(`${chalk.yellowBright(`Logs tailed for ${!!args ? args : 'not an service'}`)}\n`);
  spawn('sh', ['logs.sh', args], {
    cwd: `${path.dirname(require.main.filename)}/scripts`,
    detached: true,
    stdio: 'inherit',
  });
};
