import { spawn } from 'child_process';
import path from 'path';

export default (args) => {
  spawn('sh', ['bash.sh', args], {
    cwd: `${path.dirname(require.main.filename)}/scripts`,
    detached: true,
    stdio: 'inherit',
  });
};
