import { spawn } from 'child_process';
import chalk from 'chalk';
import path from 'path';

const installScript = () => {
  return spawn('sh', ['npm-install.sh'], {
    cwd: `${path.dirname(require.main.filename)}/scripts`,
    detached: true,
    stdio: 'inherit',
  });
};

const startScript = (args) => {
  return spawn('sh', ['start.sh', args], {
    cwd: `${path.dirname(require.main.filename)}/scripts`,
    detached: true,
    stdio: 'inherit',
  });
};

export default (args) => {
  const cloneScript = spawn('sh', ['clone-repositories.sh'], {
    cwd: `${path.dirname(require.main.filename)}/scripts`,
    detached: true,
    stdio: 'inherit',
  });
  cloneScript.on('close', (code) => {
    if (code === 0) {
      const install = installScript();
      install.on('close', (installCode) => {
        if (installCode === 0) {
          console.log(`${chalk.greenBright('Checking services')}`);
          startScript(args);
        }
      });
    }
  });
};
