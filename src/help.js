import chalk from 'chalk';

const menus = {
  main: `
${chalk.greenBright('Homify [command] <options>')}

    ${chalk.blueBright('start <app>|<>')}.......Start all homify services
    ${chalk.blueBright('stop')}.................Stop all homify services
    ${chalk.blueBright('restart <app>|<>')}.....Restart all homify services
    ${chalk.blueBright('logs <app>')}...........Tail logs for <app> service
    ${chalk.blueBright('bash <app>')}...........Log in to <app> service
    ${chalk.blueBright('gp')}...................Pull all services on git
    ${chalk.blueBright('gs')}...................Get the status of all services on git
    ${chalk.blueBright('npm install')}..........Execute npm install for each homify service
    `,
  start: `
${chalk.greenBright('Homify start <options>')}

    ${chalk.blueBright('<application>...........Initialize that particular service')}
    ${chalk.blueBright('<empty>.................Initialize all services')}
  `,
  stop: `
${chalk.greenBright('Homify stop <options>')}
  
      ${chalk.blueBright('<application>.........Stop that particular service')}
      ${chalk.blueBright('<empty>...............Stop all services')}
  `,
  restart: `
${chalk.greenBright('Homify restart <options>')}
  
      ${chalk.blueBright('<application>.........Restart that particular service')}
      ${chalk.blueBright('<empty>...............Restart all services')}
  `,
};

export default async function help(args) {
  const subCmd = args._[0] === 'help' ? args._[1] : args._[0];
  console.log(menus[subCmd] || menus.main);
}
