const packagejson = require('../package.json');

export default async function version() {
  console.log(packagejson.version);
}
