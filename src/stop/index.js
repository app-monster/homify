import { spawn } from 'child_process';
import chalk from 'chalk';
import path from 'path';

export default (args) => {
  console.log(`${chalk.greenBright('Stopping services')}`);
  spawn('sh', ['stop.sh', args], {
    cwd: `${path.dirname(require.main.filename)}/scripts`,
    detached: true,
    stdio: 'inherit',
  });
};
